package worker

import (
	"fmt"
	"math/rand"
	"os/exec"
	"sync"
)

const Key = "worker"

var workerInstance *Worker
var once sync.Once

type Tasks struct {
	sync.Mutex
	tasks map[int]*Task
}

type Worker struct {
	tasks *Tasks
}

func NewTasks() *Tasks {
	return &Tasks{
		tasks: make(map[int]*Task),
	}
}

func (t *Tasks) Add(task *Task) int {
	t.Lock()
	defer t.Unlock()
	id := rand.Int()

	t.tasks[id] = task

	return id
}

func (t *Tasks) Get(id int) (task *Task, err error) {
	t.Lock()
	defer t.Unlock()
	task, exists := t.tasks[id]
	if !exists {
		return new(Task), fmt.Errorf("cannot find task by id. id: %s", id)
	}

	return task, nil
}

func (t *Tasks) GetAll() map[int]*Task {
	return t.tasks
}

func NewWorker() *Worker {
	once.Do(func() {
		workerInstance = &Worker{NewTasks()}
	})

	return workerInstance
}

func (w *Worker) Run(t *Task) (id int, err error) {
	arg := t.Arguments
	cmdOut, err := exec.Command(t.Command, arg...).Output()
	if err != nil {
		return 0, err
	}
	t.Result = string(cmdOut)

	return w.tasks.Add(t), nil
}

func (w *Worker) GetAllTask() map[int]*Task {
	return w.tasks.tasks
}

func (w *Worker) GetTaskById(id int) (*Task, error) {
	return w.tasks.Get(id)
}
