package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/test-task/handlers"
	"gitlab.com/test-task/worker"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			workerInstance := worker.NewWorker()
			c.Set(worker.Key, workerInstance)
			return next(c)
		}
	})
	e.GET("/tasks", handlers.ListTask)
	e.GET("/tasks/:id", handlers.GetTask)
	e.POST("/tasks", handlers.CreateTask)

	e.Logger.Fatal(e.Start(":3000"))
}
