package worker

type Task struct {
	Command   string
	Arguments []string
	Result    string
}

func NewTask(command string, argument []string) *Task {
	return &Task{Command: command, Arguments: argument}
}
