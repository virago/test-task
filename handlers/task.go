package handlers

import (
	"github.com/labstack/echo"
	"gitlab.com/test-task/bindings"
	"gitlab.com/test-task/renderings"
	"gitlab.com/test-task/worker"
	"net/http"
	"strconv"
	"strings"
)

func ListTask(c echo.Context) error {
	var tasksResponse []renderings.TaskResponse
	worker := c.Get(worker.Key).(*worker.Worker)
	for uid, task := range worker.GetAllTask() {
		taskResponse := renderings.TaskResponse{
			TaskID: uid,
			Result: task.Result,
		}

		tasksResponse = append(tasksResponse, taskResponse)
	}

	return c.JSON(http.StatusOK, renderings.TasksResponse{Tasks: tasksResponse})
}

func CreateTask(c echo.Context) error {
	command := new(bindings.TaskRequest)
	if err := c.Bind(command); err != nil {
		return err
	}
	splitCommand := strings.Fields(command.Command)
	task := worker.NewTask(splitCommand[0], splitCommand[1:])

	worker := c.Get(worker.Key).(*worker.Worker)
	taskId, err := worker.Run(task)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	renderer := new(renderings.TaskResponse)
	renderer.TaskID = taskId

	return c.JSON(http.StatusOK, renderer)
}

func GetTask(c echo.Context) error {
	id := c.Param("id")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	worker := c.Get(worker.Key).(*worker.Worker)

	task, err := worker.GetTaskById(idInt)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	renderer := new(renderings.TaskResponse)
	renderer.Result = task.Result
	renderer.TaskID = idInt

	return c.JSON(http.StatusOK, renderer)
}
