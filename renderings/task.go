package renderings

type TaskResponse struct {
	TaskID int    `json:"task_id"`
	Result string `json:"result, omitempty"`
}

type TasksResponse struct {
	Tasks []TaskResponse `json:"tasks"`
}
